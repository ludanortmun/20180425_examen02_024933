# 20180425_Examen02_024933
Cada Robot es considerado como un Invoker. El método DoChore recibe un objeto Chore, todos ellos necesitan tener un Execute() de modo que pueda ser ejecutado, independientemente de cómo lo realice una instancia concreta.

El hilo principal crea un Thread por cada robot. El método a ejecutar en cada hilo se encarga de obtener un chore y realizarlo. Esto se repite hasta que no haya chores