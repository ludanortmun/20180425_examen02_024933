using System;
using System.Threading.Tasks;

namespace examen
{
    public class Robot
    {
        public int id {
            get;
        }

        public Robot(int id) 
        {
            this.id = id;
        }

        public void DoChore(Chore c)
        {
            //Run chore async
            c.Execute();

            //Add chore to log
            Console.WriteLine("Chore {0} finished by robot {1} at {2}", c.GetId(), id, DateTime.Now);
        }
    }
}