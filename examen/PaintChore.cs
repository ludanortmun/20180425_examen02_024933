using System;

namespace examen
{
    public class PaintChore : Chore
    {
        public PaintChore(int id)
        {
            this.id = id;
        }

        public override void Execute()
        {
            PaintSomething();
        }

        public void PaintSomething()
        {
            Console.WriteLine("I am painting something");
            System.Threading.Thread.Sleep(Duration);
        }

        public override int GetId()
        {
            return id;
        }
    }
}