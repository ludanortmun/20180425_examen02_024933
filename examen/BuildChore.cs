using System;

namespace examen
{
    public class BuildChore : Chore
    {
        public BuildChore(int id)
        {
            this.id = id;
        }

        public override void Execute()
        {
            BuildSomething();
        }

        public void BuildSomething()
        {
            Console.WriteLine("I am building something");
            System.Threading.Thread.Sleep(Duration);
        }

        public override int GetId()
        {
            return id;
        }
    }
}