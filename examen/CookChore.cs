using System;

namespace examen
{
    public class CookChore : Chore
    {
        public CookChore(int id)
        {
            this.id = id;
        }

        public override void Execute()
        {
            CookSomething();
        }

        public void CookSomething()
        {
            Console.WriteLine("I am cooking something");
            System.Threading.Thread.Sleep(Duration);
        }

        public override int GetId()
        {
            return id;
        }
    }
}