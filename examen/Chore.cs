namespace examen
{
    public abstract class Chore
    {
        /// <sumary>
        /// The duration, in miliseconds of a chore
        /// </summary>
        public const int Duration = 1000;
        internal int id;
        public abstract void Execute();
        public abstract int GetId();
    }
}