namespace examen
{
    public class ChoreFactory
    {
        private static ChoreFactory instance;


        private ChoreFactory() { }

        public static ChoreFactory GetInstance()
        {
            if (instance is null) {
                instance = new ChoreFactory();
            }
            return instance;
        }

        public Chore Create(int id, string kind)
        {
            Chore c = null;
            switch (kind) {
                case "paint":
                    c = new PaintChore(id);
                    break;
                case "build":
                    c = new BuildChore(id);
                    break;
                case "feed":
                    c = new FeedChore(id);
                    break;
                case "cook":
                    c = new CookChore(id);
                    break;
            }
            return c;
        }
    }
}