using System;

namespace examen
{
    public class FeedChore : Chore
    {
        public FeedChore(int id)
        {
            this.id = id;
        }

        public override void Execute()
        {
            FeedSomething();
        }

        public void FeedSomething()
        {
            Console.WriteLine("I am feeding something");
            System.Threading.Thread.Sleep(Duration);
        }

        public override int GetId()
        {
            return id;
        }
    }
}