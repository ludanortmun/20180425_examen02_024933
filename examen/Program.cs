﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace examen
{
    class Program
    {
        static List<Chore> chores;
        static Robot[] robots;
        static Object choreMutex;
        const int robotCount = 3;
        static void Main(string[] args)
        {
            chores = GetChores();
            Thread[] tasks = new Thread[robotCount];
            choreMutex = new Object();

            //Create robots
            robots = new Robot[robotCount];
            for (int i = 0; i < robotCount; i++) {
                robots[i] = new Robot(i);
            }
            //Assign chores
            foreach (Robot r in robots) {
                tasks[r.id] = new Thread (()=> AssignChore(r.id));
            }

            //Run chores
            foreach (Thread t in tasks) {
                t.Start();
            }

            //Wait for all to finish
            foreach (Thread t in tasks) {
                t.Join();
            }

            //Clear logs
            File.WriteAllText("doneTasks.csv", string.Empty);

            Console.WriteLine("Every task is done");
        }

        static void AssignChore(int robotId)
        {
            while (chores.Count > 0) {
                Chore c;
                lock (choreMutex) { 
                    c = chores.First();
                    chores.Remove(c);
                }
                if (c == null) {
                    break;
                }
                robots[robotId].DoChore(c);
                lock (choreMutex) {
                    File.AppendAllLines("doneTasks.csv", new string[] { c.GetId()+"" });
                }
            }
        }

        static List<Chore> GetChores()
        {
            ChoreFactory factory = ChoreFactory.GetInstance();
            List<Chore> chores = new List<Chore>();
            string[] choreList = File.ReadAllLines("tasks.csv");
            string[] doneChores = File.ReadAllLines("doneTasks.csv");

            foreach (string s in choreList) {
                string choreId = s.Split(',')[0];
                string task = s.Split(',')[1];
                if (doneChores.Contains(choreId)) {
                    continue;
                }
                chores.Add(factory.Create(int.Parse(choreId), task));
            }

            return chores;
        }
    }
}
